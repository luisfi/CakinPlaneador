; debug
; (watch facts)
; (watch rules)
; (watch activations)

; templates
(deftemplate goal (slot from-room) (slot to-room))
(deftemplate in-room (slot room))
    
(deffacts rooms "The rooms of the house"
    (room kitchen)
    (room bedroom)
    (room corridor)
    (room service)
    (room studio)
    (room deposit))

(deffacts initial-state "The room where is initially the robot"
    (in-room (room deposit))
    (goal (from-room deposit) (to-room kitchen)))

(defrule go-directly "From the corridor the robot can go to any room"
    ?goal <- (goal (from-room corridor) (to-room ?roomf))
    (room ?roomf)
    ?inside <- (in-room (room corridor))
    =>
    (retract ?goal ?inside)
    (assert (in-room (room ?roomf)))
    (printout t "Opening the door of " ?roomf crlf)
    (printout t "Closing the door of " ?roomf crlf)
    (printout t "Now on " ?roomf crlf))

(defrule goto-corridor "Go to the corridor before going to other room"
    ?goal <- (goal (from-room ?roomi) (to-room ?roomf))
    (room ?roomi)
    (room ?roomf)
    ?inside <- (in-room (room ?roomi))
    =>
    (retract ?goal ?inside)
    (assert 
        (goal (from-room corridor) (to-room ?roomf))
        (in-room (room corridor)))
    (printout t "Opening the door of " ?roomi crlf)
    (printout t "Closing the door of " ?roomi crlf)
    (printout t "Now on corridor" crlf))

; start the program

