;***********************************************************
;*                 Templates definition                    *
;***********************************************************

(deftemplate item
  (slot type)
  (slot name)
  (slot zone)
  (slot image)
  (slot attributes)
  (multislot pose)
  (slot belongs)
  (slot status)
)

(deftemplate human
  (slot name)
  (slot zone)
  (slot image)
  (slot attributes)
  (multislot pose)
  (slot carring)
  (slot status)
)

(deftemplate robot
  (slot name)
  (slot zone)
  (slot image)
  (slot attributes)
  (multislot pose)
  (slot carring)
  (slot status)
)


;***********************************************************
;*         Environment initial state                       *
;***********************************************************

(deffacts Initial-state-objects-rooms-zones-actors
  ; Objects
  (item (type Objects) (name apple) (zone corridor) (image apple) (attributes pick) (pose 0.08 0.57 2.0))
  (item (type Objects) (name sushi) (zone corridor) (image sushi) (attributes pick) (pose 0.08 0.57 1.0))
  (item (type Objects) (name milk) (zone corridor) (image milk) (attributes pick) (pose 0.08 0.57 0.0))
  (item (type Objects) (name soap) (zone corridor) (image soap) (attributes pick) (pose 0.07 0.52 0.0))
  (item (type Objects) (name shampoo) (zone corridor) (image shampoo) (attributes pick) (pose 0.07 0.52 0.0))
  (item (type Objects) (name book) (zone deposit) (image book) (attributes pick) (pose 0.22 1.10 0.0))
  (item (type Objects) (name hammer) (zone deposit) (image hammer) (attributes pick) (pose 0.22 1.10 2.0))

  ; Rooms
  (item (type Room) (name deposit) (pose 0.29 0.93 0.0))
  (item (type Room) (name kitchen) (pose 0.68 1.10 0.0))
  (item (type Room) (name corridor) (pose 0.39 0.58 0.0))
  (item (type Room) (name studio) (pose 0.13 0.24 0.0))
  (item (type Room) (name bedroom) (pose 0.40 0.28 0.0))
  (item (type Room) (name service) (pose 0.67 0.29 0.0))

  ; Doors
  (item (type Door) (name fridgedoor) (zone kitchen) (status closed) (belongs fridge) (pose 0.72 0.89 0.0))
  (item (type Door) (name entrance) (zone corrido) (status closed) (belongs corridor_wall) (pose 0.6 0.57 0.0))

  ; Furniture
  (item (type Furniture) (name bedroom_table) (zone bedroom) (pose 0.38 0.16 0.0))
  (item (type Furniture) (name deposit_table) (zone deposit) (pose 0.19 1.13 0.0))
  (item (type Furniture) (name fridge) (zone kitchen) (pose 0.65 0.82 0.0))
  (item (type Furniture) (name service_table) (zone service) (pose 0.63 0.17 0.0))

  ; Humans
  (human (name Mother) (zone bedroom) (pose 0.59 0.25 0.0))
  (human (name Father) (zone service) (pose 0.73 0.21 0.0))

  ; Robot
  (robot (name Justina) (zone studio) (pose 0.09 0.16 0.0))
  )
