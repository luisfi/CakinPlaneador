
; templates
(deftemplate goal
    (slot from-room)
    (slot to-room)
)
(deftemplate in-room
    (slot room)
)
(deftemplate movable-object
    (slot name)
    (slot to-room))

(deftemplate on-top-of
  (slot upper)
  (slot lower)
)

(deftemplate item
  (slot type)
  (slot name)
  (slot zone)
  (slot image)
  (slot attributes)
  (multislot pose)
  (slot belongs)
  (slot status)
)

(deftemplate human
  (slot name)
  (slot zone)
  (slot image)
  (slot attributes)
  (multislot pose)
  (slot carring)
  (slot status)
)

(deftemplate robot
  (slot name)
  (slot zone)
  (slot image)
  (slot attributes)
  (multislot pose)
  (slot carring)
  (slot status)
)

(deftemplate changed-pose
    (slot initial)
    (slot final)
)

(deftemplate take-object
    (slot object)
    (slot to))



(deffacts Initial-state-objects-rooms-zones-actors
  ; Objects
  (item (type Objects) (name apple) (zone corridor) (image apple) (attributes pick) (pose 0.08 0.57 2.0))
  (item (type Objects) (name sushi) (zone corridor) (image sushi) (attributes pick) (pose 0.08 0.57 1.0))
  (item (type Objects) (name milk) (zone corridor) (image milk) (attributes pick) (pose 0.08 0.57 0.0))
  (item (type Objects) (name soap) (zone corridor) (image soap) (attributes pick) (pose 0.07 0.52 1.0))
  (item (type Objects) (name shampoo) (zone corridor) (image shampoo) (attributes pick) (pose 0.07 0.52 0.0))
  (item (type Objects) (name book) (zone deposit) (image book) (attributes pick) (pose 0.22 1.10 0.0))
  (item (type Objects) (name hammer) (zone deposit) (image hammer) (attributes pick) (pose 0.22 1.10 1.0))

  ; Rooms
  (item (type Room) (name deposit) (pose 0.29 0.93 0.0))
  (item (type Room) (name kitchen) (pose 0.68 1.10 0.0))
  (item (type Room) (name corridor) (pose 0.39 0.58 0.0))
  (item (type Room) (name studio) (pose 0.13 0.24 0.0))
  (item (type Room) (name bedroom) (pose 0.40 0.28 0.0))
  (item (type Room) (name service) (pose 0.67 0.29 0.0))

  ; Doors
  (item (type Door) (name fridgedoor) (zone kitchen) (status closed) (belongs fridge) (pose 0.72 0.89 0.0))
  (item (type Door) (name entrance) (zone corrido) (status closed) (belongs corridor_wall) (pose 0.6 0.57 0.0))

  ; Furniture
  (item (type Furniture) (name bedroom_table) (zone bedroom) (pose 0.38 0.16 0.0))
  (item (type Furniture) (name deposit_table) (zone deposit) (pose 0.19 1.13 0.0))
  (item (type Furniture) (name fridge) (zone kitchen) (pose 0.65 0.82 0.0))
  (item (type Furniture) (name service_table) (zone service) (pose 0.63 0.17 0.0))

  ; Humans
  (human (name Mother) (zone bedroom) (pose 0.59 0.25 0.0))
  (human (name Father) (zone service) (pose 0.73 0.21 0.0))

  ; Robot
  (robot (name Justina) (zone studio) (pose 0.09 0.16 0.0) (status idle))
  )


(deffacts rooms "The rooms of the house"
    (room kitchen)
    (room bedroom)
    (room corridor)
    (room service)
    (room studio)
    (room deposit)
)

(deffacts initial-state "The room where is initially the robot"
    (in-room (room studio))
    ;(goal (from-room deposit) (to-room kitchen))
)
(defrule go-directly "From the corridor the robot can go to any room"
    ?goal <- (goal (from-room corridor) (to-room ?room))
    (room ?room)
    (item (type Room) (name ?room) (pose $?pose_room))
    ?inside <- (in-room (room corridor))
    ?robot <- (robot)
    =>
    (retract ?goal ?inside)
    (assert (in-room (room ?room)))
    (modify ?robot (pose ?pose_room) (zone ?room))
    (printout t "goto " ?room crlf)
    (printout t "goto " ?pose_room crlf)
    (printout t "Opening the door of " ?room crlf)
    (printout t "Closing the door of " ?room crlf)
    (printout t "Now on " ?room " " ?pose_room crlf))

(defrule goto-corridor "Go to the corridor before going to other room"
    ?goal <- (goal (from-room ?roomi) (to-room corridor))
    (room ?roomi)
    (room ?roomf)
    ?inside <- (in-room (room ?roomi))
    ?robot <- (robot)
    (item (type Room) (name corridor) (pose $?pose_corridor))
    =>
    (retract ?goal ?inside)
    (modify ?robot (pose ?pose_corridor) (zone corridor))
    (assert
        (in-room (room corridor)))
    (printout t "goto corridor" crlf)
    (printout t "goto " ?pose_corridor crlf)
    (printout t "Opening the door of " ?roomi crlf)
    (printout t "Closing the door of " ?roomi crlf)
    (printout t "Now on corridor " ?pose_corridor crlf))

(defrule goto-items "Goto the objects position"
        (in-room (room ?room&~corridor))
        (room ?room)
    =>
        (assert (goal (from-room ?room) (to-room corridor)))
        (printout t "In room " ?room crlf)
        (printout t "Moving to corridor" crlf)
)

(defrule goto-entrance "Go to the objects position"
        (in-room (room corridor))
        ?robot <- (robot (status idle) (pose $?pose_robot))
        (item (type Door) (name entrance) (pose $?pose_entrance))
        (test (neq ?pose_robot ?pose_entrance)) ; Debido a que se modifica datos del robot
    =>
        (modify ?robot (pose ?pose_entrance))
        (printout t "goto " ?pose_entrance " (Door entrance)" crlf)
)

(defrule move-table-items "Mueve los objetos que pertenecen a la mesa de servicio"
        ?item <- (item (name ?n&soap|shampoo) (zone corridor) (pose $?po))
        (item (type Furniture) (name service_table) (pose $?pf))
        (robot (pose $?pose_robot))
        (item (type Door) (name entrance) (pose $?pose_entrance))
        (test (eq ?pose_robot ?pose_entrance))
        (on-top-of (upper nothing) (lower ?n))
    =>
        (assert (movable-object (name ?n) (to-room service)))
        (printout t "There is " ?n " in corridor " ?po " and should be moved to " ?pf
crlf)
)

(defrule move-fridge-items "Mueve los objetos que pertenecen al refrigerador"
        ?item <- (item (name ?n&apple|sushi|milk) (zone corridor) (pose $?po))
        (item (type Furniture) (name fridge) (pose $?pf))
        (robot (pose $?pose_robot))
        (item (type Door) (name entrance) (pose $?pose_entrance))
        (test (eq ?pose_robot ?pose_entrance))
        (on-top-of (upper nothing) (lower ?n))
    =>
        (assert (movable-object (name ?n) (to-room kitchen)))
        (printout t "There is " ?n " in corridor " ?po " and should be moved to " ?pf
         crlf)
)

; Rules
(defrule add-top-of
    (declare (salience 2))
    (item (type Objects) (name ?item1) (pose ?x ?y ?z))
    (item (type Objects) (name ?item2) (pose ?x ?y ?z2))
    (test (= ?z2 (+ ?z 1.0)))
  =>
    (assert (on-top-of (upper ?item2) (lower ?item1)))
    (printout t "The item " ?item2 " is on top of " ?item1 crlf)
)

(defrule add-upper-nothing
    (declare (salience 1))
    (item (type Objects) (name ?item1) (pose ?x ?y ?z))
    (item (type Objects) (name ?item2) (pose ?x ?y ?z2))
    (test (< ?z ?z2))
    (not (on-top-of (lower ?item2)))
  =>
    (assert (on-top-of (upper nothing) (lower ?item2)))
    (printout t "Nothing is on top of " ?item2 " Compared to " ?item1 crlf)
)

(defrule add-upper-nothing-alone
    (declare (salience 1))
    (item (type Objects) (name ?item1) (pose ?x ?y ?z))
    (test (= ?z 0.0))
    (not (on-top-of (lower ?item1)))
  =>
  (assert (on-top-of (upper nothing) (lower ?item1)))
    (printout t "Nothing is on top of " ?item1 crlf)
)

(defrule add-lower-floor
    (declare (salience 1))
    (item (type Objects) (name ?item1) (pose ?x ?y ?z))
    (test (= ?z 0.0))
  =>
  (assert (on-top-of (upper ?item1) (lower floor)))
    (printout t ?item1 " is upper of floor" crlf)
)

(defrule move-to-fridge
        (in-room (room kitchen))
        ?robot <- (robot (status busy) (pose $?pose_robot))
        (item (type Room) (name kitchen) (pose $?pose_kitchen))
        (test (eq ?pose_robot ?pose_kitchen))
        (item (name fridgedoor) (pose $?pose_fridge))
    =>
        (modify ?robot (pose ?pose_fridge))
        (printout t "goto " ?pose_fridge " (fridge)" crlf)
)

(defrule move-to-table
        (in-room (room service))
        ?robot <- (robot (status busy) (pose $?pose_robot))
        (item (type Room) (name service) (pose $?pose_room))
        (test (eq ?pose_robot ?pose_room))
        (item (name service_table) (pose $?pose_table))
    =>
        (modify ?robot (pose ?pose_table))
        (printout t "goto " ?pose_table "(table)" crlf)
)


(defrule release-item-table
        (in-room (room  service))
        ?robot <- (robot (status busy) (pose $?pose_robot))
        (item (name service_table) (pose $?pose_table))
        (test (eq ?pose_table ?pose_robot))
        ?moving <-  (moving ?n)
        ?item <- (item (type Objects) (name ?n))
        ?top <- (on-top-of (upper ?n) (lower ?n2))
    =>
        (modify ?robot (status idle))
        (modify ?item (zone service) (pose ?pose_table))
        (retract ?moving)
        (retract ?top)
        (assert (on-top-of (upper nothing) (lower ?n2)))
        (printout t "release " ?n crlf)
)


(defrule release-item-fridge
        (in-room (room  kitchen))
        ?robot <- (robot (status busy) (pose $?pose_robot))
        (item (name fridgedoor) (pose $?pose_door))
        (test (eq ?pose_door ?pose_robot))
        ?moving <- (moving ?n)
        ?item <- (item (type Objects) (name ?n))
        ?top <- (on-top-of (upper ?n) (lower ?n2))
    =>
        (modify ?robot (status idle))
        (modify ?item (zone kitchen) (pose ?pose_door))
        (retract ?moving)
        (retract ?top)
        (assert (on-top-of (upper nothing) (lower ?n2)))
        (printout t "open door" crlf)
        (printout t "release " ?n crlf)
        (printout t "close door" crlf)
)

(defrule move-object
        ?mov <- (movable-object (name ?n) (to-room ?room))
        ?robot <- (robot)
    =>
        (retract ?mov)
        (assert (goal (from-room corridor) (to-room ?room)))
        (assert (moving ?n))
        (modify ?robot (status busy))
        (printout t "grasp " ?n crlf)
)
